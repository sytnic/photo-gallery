<?php require_once("../../includes/initialize.php"); ?>
<?php if (!$session->is_logged_in()) { redirect_to("login.php"); } ?>
<?php
	// must have an ID
  if(empty($_GET['id'])) {
  	$session->message("No photograph ID was provided.");
    redirect_to('index.php');
  }

  // возвращается 1 объект
  $photo = Photograph::find_by_id($_GET['id']);

  // основная работа выполняется в $photo->destroy и сразу возвращается булево
  if($photo && $photo->destroy()) {
      // объект $photo с атрибутами остаётся до конца выполнения скрипта,
      // несмотря на то, что запись в БД, на к-рой он был основан, уже удалена
    $session->message("The photo {$photo->filename} was deleted.");
    redirect_to('list_photos.php');
  } else {
    $session->message("The photo could not be deleted.");
    redirect_to('list_photos.php');
  }
  
?>
<?php if(isset($database)) { $database->close_connection(); } ?>
