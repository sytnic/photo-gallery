<?php 
require_once('../includes/initialize.php');
include_layout_template('header.php');
?>
<?php
  // Find all the photos
  $photos = Photograph::find_all();
?>

<?php foreach($photos as $photo): ?>
    <div>
      <a href="photo_mine.php?id=<?php echo $photo->id; ?>">
        <img src="<?php echo $photo->image_path(); ?>" width="200" />
      </a>  
        <p> <?php echo $photo->caption; ?> </p>
    </div>
<?php endforeach; ?>



<?php
include_layout_template('footer.php');
?>