<?php
// A class to help work with Sessions
// In our case, primarily to manage logging users in and out

// Keep in mind when working with sessions that it is generally 
// inadvisable to store DB-related objects in sessions

/* Минимальный набор для работы сессии

class Session {
    function __construct() {
		session_start();
    }
}

$session = new Session();

*/

class Session {
 
    private $logged_in=false;
  	public $user_id;
    public $message;
 
    function __construct() {
		    session_start();
        
        // проверка, было ли задано сообщение при редиректе,
        // т.о. public $message всегда получает либо пустую строку, 
        // либо значение из $_SESSION['message']
        $this->check_message();

        // проверка залогинености        
        $this->check_login();
        
        // Если авторизован (logged_in==true),
        // сделать один набор действий,
        // иначе другой набор.
        if($this->logged_in) {
          // actions to take right away if user is logged in
        } else {
          // actions to take right away if user is not logged in
        }
    }
    
    // возвращает булево значение (true/false)
    // возвращает $session->logged_in как true или false, что устанавливается
    // с помощью check_login() через __construct
    public function is_logged_in() {
        return $this->logged_in;
    }
    
    // используется объект $user из БД
    // присваиваются значения атрибутам объекта $session
    public function login($user) {
        // database should find user based on username/password
        if($user){
          $this->user_id = $_SESSION['user_id'] = $user->id;
          $this->logged_in = true;
        }
    }    
    
    public function logout() {
        unset($_SESSION['user_id']);
        unset($this->user_id);
        $this->logged_in = false;
    }

    // задать строку $msg в сессию
    // или вернуть то, что есть в $this->message,
    // $this->message через construct всегда задаётся в check_message()
    // и там может получить пустую строку или взять значение из $_SESSION['message']
    public function message($msg="") {
      if(!empty($msg)) {
        // then this is "set message"
        // make sure you understand why $this->message=$msg wouldn't work
        // - выражение $this->message=$msg присвоило бы значение атрибуту message,
        // но не сохранило бы его в реальную сессию.
        $_SESSION['message'] = $msg;
      } else {
        // then this is "get message"
        return $this->message;
      }
    }
    
    private function check_login() {
        // Если сессия задана,
        // присвоить значения переменным объекта,
        // иначе стереть значения
        if(isset($_SESSION['user_id'])) {
          $this->user_id = $_SESSION['user_id'];
          $this->logged_in = true;
        } else {
          unset($this->user_id);
          $this->logged_in = false;
        }
    }

    private function check_message() {
      // Is there a message stored in the session?
      if(isset($_SESSION['message'])) {
        // Add it as an attribute and erase the stored version
        $this->message = $_SESSION['message'];
        unset($_SESSION['message']);
      } else {
        $this->message = "";
      }
    }


    
}

$session = new Session();
// здесь атрибут $this->message благодаря construct и check_message() 
// получает либо "", либо текущее $_SESSION['message']

// так в переменную $message будет получено то, что есть в атрибуте $this->message:
$message = $session->message();
// тем не менее атрибут $this->message с каждым вызовом любой страницы перезатирается, т.к.
// здесь отрабатывает construct и check_message() при вызове new Session()
// в зависимости от того, есть ли что-то в $_SESSION['message'] или оно пусто

// если параметр $session->message('строка') указан, то эта строка присвоится к $_SESSION['message'],
// но ничего не будет возвращено, 
// т.е. функция выполнит работу, но к переменной $message ничего не будет присвоено (она станет NULL)

// Т.о. $session->message() всегда получает значение атрибута $this->message, словно $session->get_message(),
// а $session->message('с параметром') всегда задаёт значение $_SESSION['message'], к-рое в construct и check_message()
// будет на один раз (одну загрузку страницы) присвоено к $this->message, т.е. словно $session->set_message() .

// чтобы понять, как работает, можно добавить в какой-нибудь файл
/*
      var_dump($message);
      var_dump($session->message()); // эта строка сразу выполняет работу
                                    // и если не указан параметр, возвращает текущее значение $this->message 
*/
// а здесь выше добавить любую строку в параметр:
// $message = $session->message('anyword');


?>