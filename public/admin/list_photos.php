<?php require_once("../../includes/initialize.php"); ?>
<?php if (!$session->is_logged_in()) { redirect_to("login.php"); } ?>
<?php
  // Find all the photos
  $photos = Photograph::find_all();
?>
<?php include_layout_template('admin_header.php'); ?>

<h2>Photographs</h2>

<p>Картинка берётся из файловой системы, а значения изначально из базы данных, затем (для страницы) из объекта.</p>
<p>Поэтому при удалении картинки нужно удалять картинку как файл и также данные о ней из базы данных.</p>

<?php echo output_message($message); 
      // $message подхватится 
      // из session.php в 120 строке $message = $session->message() 
      // и возьмет свое значение из $session->message("Photograph... в 23 строке в photo_upload.php 
      // или из аналогичного файла
      // либо получит пустую строку, если сообщение в сессию не будет задано .
?>

<table class="bordered">
  <tr>
    <th>Image</th>
    <th>Filename</th>
    <th>Caption</th>
    <th>Size</th>
    <th>Type</th>
    <th>Comments</th>
    <th>&nbsp</th>
  </tr>
<?php foreach($photos as $photo): ?>
  <tr>
    <td><img src="../<?php echo $photo->image_path(); ?>" width="100" /></td>
    <td><?php echo $photo->filename; ?></td>
    <td><?php echo $photo->caption; ?></td>
    
    <?php   echo "<td>".$photo->size_as_text()."</td>"; ?>
    
    <?php // как вариант:
          //  echo "<td>"; 
          //  echo $photo->size > 1024 ? round ($photo->size / 1024) ." Kb" : $photo->size ." bytes"."</td>" ;
    ?>

    <td><?php echo $photo->type; ?></td>
    <td>
        <a href="comments.php?id=<? echo $photo->id; ?>"> 
          <?php echo count($photo->comments());  ?>
        </a>
    </td>
    <td> <a href="delete_photo.php?id=<?php echo $photo->id; ?>">Delete</a> </td>
  </tr>
<?php endforeach; ?>
</table>
<br />
<a href="photo_upload.php">Upload a new photograph</a>

<?php include_layout_template('admin_footer.php'); ?>