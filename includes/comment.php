<?php

// If it's going to need the database, then it's 
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class Comment extends DatabaseObject {

    protected static $table_name="comments";
    protected static $db_fields=array('id', 'photograph_id', 'created', 'author', 'body');

    public $id;
    public $photograph_id;
    public $created;
    public $author;
    public $body;

    /**
     * @return object|false возвращается объект класса Comment или ложь
     */
    // "new" is a reserved word so we use "make" (or "build")
    public static function make($photo_id, $author="Anonymous", $body="") {
        if(!empty($photo_id) && !empty($author) && !empty($body)) {
            $comment = new Comment();
            $comment->photograph_id = (int)$photo_id;
            $comment->created = strftime("%Y-%m-%d %H:%M:%S", time());
            $comment->author = $author;
            $comment->body = $body;
            return $comment;
        } else {
            return false;
        }
    }

    // вернётся массив всех объектов комментариев на основе таблицы comments
    // согласно условию (соответствия внешнего ключа) в SQL-запросе
    // (на основе данных из БД присвоятся аналогичные атрибуты каждому объекту,
    //  сам объект (поштучно) объявляется в методе DatabaseObject::instantiate
    //  как $object = new static;
    //  массив из поштучных объектов создаётся в цикле while в DatabaseObject::find_by_sql)
    // 
    /**
     * @return array of objects
     */
    public static function find_comments_on($photo_id=0) {
        global $database;
        $sql = "SELECT * FROM " . self::$table_name;
        $sql .= " WHERE photograph_id=" .$database->escape_value($photo_id);
        $sql .= " ORDER BY created ASC";
        return self::find_by_sql($sql);
    }

    // Для работы функции 
    // 1 - скачан PHPMailer как zip архив
    // и помещен без изменений как папка в includes 
    // https://github.com/PHPMailer/PHPMailer
    // 2 - нужно указать реальные почты и пароль почты авторизации на сервере smtp
    public function try_to_send_notification() {

		$mail = new PHPMailer\PHPMailer\PHPMailer();

		$mail->isSMTP();
		$mail->Host     = "smtp.mail.ru";
		$mail->Port     = 25;
		$mail->SMTPAuth = true;
		$mail->Username = "login@mail.ru";
		$mail->Password = "password";

		$mail->FromName = "Photo Gallery";
		$mail->From     = "from@mail.ru";
        // $mail->addAddress($to, $to_name);
		$mail->addAddress("to@mail.ru", "Photo Gallery Admin");
		$mail->Subject  = "New Photo Gallery Comment. ";

        $created = datetime_to_text($this->created);
        $mail->Body     =<<<ANYWORD

A new comment has been received in the Photo Gallery.

    At {$created}, {$this->author} wrote:
    
{$this->body}
        
ANYWORD;    

		$result = $mail->send();
		return $result;
    }
}


?>