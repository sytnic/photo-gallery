<?php require_once('../../includes/initialize.php'); ?>
<?php if (!$session->is_logged_in()) { redirect_to("login.php"); } ?>

<?php

$photo = Photograph::find_by_id($_GET['id']);

// массив объектов комментариев
$comments = $photo->comments();

?>


<?php include_layout_template('admin_header.php'); ?>

<div id="comments">
  <?php foreach($comments as $comment): ?>
    <div class="comment" style="margin-bottom: 2em;">
	    <div class="author">
	      <?php echo htmlentities($comment->author); ?> wrote:
	    </div>
      <div class="body">
				<?php echo strip_tags($comment->body, '<strong><em><p>'); ?>
			</div>
	    <div class="meta-info" style="font-size: 0.8em;">
	      <?php echo datetime_to_text($comment->created); ?>
	    </div>
        <div class="meta-info" >
	      <a href=""> Delete  </a>
	    </div>
    </div>
  <?php endforeach; ?>
  <?php if(empty($comments)) { echo "No Comments."; } ?>
</div>



<?php include_layout_template('admin_footer.php'); ?>