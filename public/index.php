<?php require_once('../includes/initialize.php'); ?>
<?php

  // Следующие три переменные нужны для пагинации

  // 1. the current page number ($current_page)
  $page = !empty($_GET['page']) ? (int)$_GET['page'] : 1;

  // 2. records per page ($per_page)
  $per_page = 3;

  // 3. total record count ($total_count)
  $total_count = Photograph::count_all();

  // $total_count - это строка:
  //echo $total_count."<br>";
  //var_dump($total_count);

	// Find all photos
  // use pagination instead
	// $photos = Photograph::find_all();

  $pagination = new Pagination($page, $per_page, $total_count);
	
	// Instead of finding all records, just find the records 
	// for this page
	$sql = "SELECT * FROM photographs ";
	$sql .= "LIMIT {$per_page} ";
  // offset uses $page and $per_page
	$sql .= "OFFSET {$pagination->offset()}";
  
	$photos = Photograph::find_by_sql($sql);

  // Need to add ?page=$page to all links we want to 
	// maintain the current page (or store $page in $session)

?>

<?php include_layout_template('header.php'); ?>

<?php echo output_message($message); 
		    // $message подхватится 
        // из session.php в 120 строке $message = $session->message() 
        // и возьмет свое значение из $session->message("какой-то текст...") в photo.php, например,
        // либо получит пустую строку, если сообщение в сессию не будет задано .
?>

<?php foreach($photos as $photo): ?>
  <div style="float: left; margin-left: 20px;">
		<a href="photo.php?id=<?php echo $photo->id; ?>">
			<img src="<?php echo $photo->image_path(); ?>" width="200" />
		</a>
    <p><?php echo $photo->caption; ?></p>
  </div>
<?php endforeach; ?>

<div id="pagination" style="clear: both;">
<?php
  if($pagination->total_pages() > 1) {
		
    // if(bool)
		if($pagination->has_previous_page()) { 
    	echo "<a href=\"index.php?page=";
      echo $pagination->previous_page();
      echo "\">&laquo; Previous</a> "; 
    }

    for($i=1; $i <= $pagination->total_pages(); $i++) {
      // если число $i, по к-рому мы проходим, равна числу $page (основана на $_GET['page']),
      // то выводим span class, a не ссылку   
      if($i == $page) {
				echo " <span class=\"selected\">{$i}</span> ";
			} else {
      // иначе, в остальных случаях, выводим ссылку
        echo  "<a href=\"index.php?page={$i}\">{$i}</a> ";
      }
    }

    // if(bool)
    if($pagination->has_next_page()) { 
			echo " <a href=\"index.php?page=";
			echo $pagination->next_page();
			echo "\">Next &raquo;</a> "; 
    }
  }
?>
</div>

<?php include_layout_template('footer.php'); ?>