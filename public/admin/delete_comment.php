<?php require_once("../../includes/initialize.php"); ?>
<?php if (!$session->is_logged_in()) { redirect_to("login.php"); } ?>
<?php
	// must have an ID
  if(empty($_GET['id'])) {
  	$session->message("No comment ID was provided.");
    redirect_to('index.php');
  }

  // объект или false
  $comment = Comment::find_by_id($_GET['id']);
  
  // delete() удаляет SQL-запросом и возвращает булево
  if($comment && $comment->delete()) {
    $session->message("The comment was deleted.");
    // запись удалена из БД, но объект $comment пока есть на этой странице,
    // поэтому возможно обращение к атрибуту
    redirect_to("comments.php?id={$comment->photograph_id}");
    // иначе редирект без действий
  } else {
    $session->message("The comment could not be deleted.");
    redirect_to('list_photos.php');
  }
  
?>
<?php if(isset($database)) { $database->close_connection(); } ?>