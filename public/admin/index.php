<?php

require_once('../../includes/initialize.php');

// возвращает булево значение  
// возвращает $session->logged_in как true или false 
if (!$session->is_logged_in()) { redirect_to("login.php"); }
?>
<?php include_layout_template('admin_header.php'); ?>
		<h2>Menu</h2>

		<?php echo output_message($message); 
		    // $message подхватится 
            // из session.php в 120 строке $message = $session->message() 
            // и возьмет свое значение из $session->message("какой-то текст...") в delete_photo.php, например,
            // либо получит пустую строку, если сообщение в сессию не будет задано .
		?>
			<ul>
				<li><a href="list_photos.php">List Photos</a></li>
				<li><a href="logfile.php">View Log file</a></li>
				<li><a href="logout.php">Logout</a></li>
			</ul>
		
<?php include_layout_template('admin_footer.php'); ?>		
    