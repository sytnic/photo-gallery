-- mysql -u gallery -p (php123)
-- show databases;
-- use photo_gallery;
-- show tables;


CREATE TABLE `photographs` (
  `id` int(11) NOT NULL auto_increment,
  `filename` varchar(255) NOT NULL,
  `type` varchar(100) NOT NULL,
  `size` int(11) NOT NULL,
  `caption` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
);

-- SHOW FIELDS FROM this_table;