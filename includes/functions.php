<?php

function strip_zeros_from_date($marked_string="") {
        // remove the marked zeros
        // ex. for *01
        $no_zeros = str_replace('*0', '', $marked_string);
        // then remove any remaining marks
        // ex. for *12
        $cleaned_string = str_replace('*', '', $no_zeros);
        return $cleaned_string;
	}
    
function redirect_to( $location = NULL ) {
  if ($location != NULL) {
    header("Location: {$location}");
    exit;
  }
}

function output_message($message="") {
  if (!empty($message)) { 
    return "<p class=\"message\">{$message}</p>";
  } else {
    return "";
  }
}

/*  // Deprecated
 function __autoload($class_name) { 
   
	$class_name = strtolower($class_name);
      $path = "../includes/{$class_name}.php";
      if(file_exists($path)) {
        require_once($path);
      } else {
            die("The file {$class_name}.php could not be found.");
        }
}
*/
spl_autoload_register(function ($class_name) {
      $class_name = strtolower($class_name);
      $path = LIB_PATH."{$class_name}.php";
        if(file_exists($path)) {
            require_once($path);
        } else {
            die("The file {$class_name}.php could not be found.");
        }
    });
  
function include_layout_template($template="") {
	include(SITE_ROOT.'/public/layouts/'.$template);
}        

/**
 * @param string $action
 * @param string $message
 * @return void записывает в лог или возвращает сообщение
 */
function log_action($action, $message="") {
	$logfile = SITE_ROOT.'/logs/log.txt';
	$new = file_exists($logfile) ? false : true;
  if($handle = fopen($logfile, 'a')) { // append 
    // 'a' - добавление в конец файла, плюс его создание, если он не существует
    $timestamp = strftime("%Y-%m-%d %H:%M:%S", time());
		$content = "{$timestamp} | {$action}: {$message}\n";
    fwrite($handle, $content);
    fclose($handle);
    if($new) { chmod($logfile, 0755); } // если файл новый, то назначить права для записи в него
  } else {
    echo "Could not open log file for writing.";
  }
}

function datetime_to_text($datetime="") {
  // сначала время - во временную метку unix
  $unixdatetime = strtotime($datetime); 
  // теперь временная метка unix - в нужную строку
  return strftime("%B %d, %Y at %I:%M %p", $unixdatetime);
}


?>