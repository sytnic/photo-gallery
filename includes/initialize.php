<?php
// Define the core paths
// Define them as absolute paths to make sure that require_once works as expected

// DIRECTORY_SEPARATOR is a PHP pre-defined constant
// (\ for Windows, / for Unix)

defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);
/* Не стал использовать DS здесь, но определил её

defined('SITE_ROOT') ? null : 
	define('SITE_ROOT', DS.'Users'.DS.'kevin'.DS.'Sites'.DS.'photo_gallery');

defined('LIB_PATH') ? null : define('LIB_PATH', SITE_ROOT.DS.'includes');
*/
defined('SITE_ROOT') ? null : 
	define('SITE_ROOT', 'E:\ospanel\7.4\openserver\domains\photo-gallery');

// со слэшем в конце
defined('LIB_PATH') ? null : define('LIB_PATH', SITE_ROOT.'/includes/');    

// load config
require_once(LIB_PATH."config.php");

// load functions
require_once(LIB_PATH."functions.php");

// load core objects
require_once(LIB_PATH."session.php");
require_once(LIB_PATH."database.php");
require_once(LIB_PATH."database_object.php");
require_once(LIB_PATH."pagination.php");

require_once(LIB_PATH."phpMailer/src/PHPMailer.php");
require_once(LIB_PATH."phpMailer/src/SMTP.php");
require_once(LIB_PATH."phpMailer/language/phpmailer.lang-ru.php");	


// load database-related classes
require_once(LIB_PATH."user.php");
require_once(LIB_PATH."photograph.php");
require_once(LIB_PATH."comment.php");


?>