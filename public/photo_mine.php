<?php 
require_once('../includes/initialize.php');
include_layout_template('header.php');
?>
<?php
	// Find photo by id
    // returns 1 object
	$photo = Photograph::find_by_id($_GET['id']);
?>

<img src="<?php echo $photo->image_path(); ?>" width="400" />
<p> <?php echo $photo->caption ?> </p>
<p>
<a href="index_mine.php">Home</a>
</p>

<?php
include_layout_template('footer.php');
?>