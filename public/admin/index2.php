<?php // My sandbox page

require_once('../../includes/initialize.php');

// возвращает $session->logged_in как true или false 
//if (!$session->is_logged_in()) { redirect_to("login.php"); }
?>
<html>
  <head>
    <title>Photo Gallery</title>
    <link href="../stylesheets/main.css" media="all" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <div id="header">
      <h1>Photo Gallery</h1>
    </div>
    <div id="main">
    
<?php
// возвращает булево значение  
// возвращает $session->logged_in как true или false 
var_dump($session->is_logged_in()); ?>

		<h2>Menu</h2>
        
        <pre>
<?php
    // authenticate возвращает false, если пользователь не найден
    $found_user = User::authenticate("kskoglund", "secret-pwd");
    var_dump($found_user); // User Object or bool(false)
    
    
    if($found_user) {
        echo 'if($found_user) возвращает true'."<br>"; 
    } else {
        echo 'if($found_user) возвращает false'."<br>";
    }
    
        
        $sql  = "SELECT * FROM users ";        
        
        $result_set = $database->query($sql);
        //вызывается $result = mysqli_query($database->connection, $sql);        
        print_r($result_set); // mysqli_result Object
		
        $object_array = array();
        
        echo "<br>";
        while ($row = $database->fetch_array($result_set)) {
          print_r($row); // mix array from db
          //$object_array[] = User::instantiate($row); работает так:
          /*  $object = new User;
          
          // has_attribute() private!
          
            foreach($row as $attribute=>$value){
              if($object->has_attribute($attribute)) {
                $object->$attribute = $value;
              }
            }
        
            $object_array[] = $object;
          */
          
          // или, что схоже, так:
           $object = new User;
           $object->id         = $row['id'];
           $object->username   = $row['username'];
           $object->password   = $row['password'];
           $object->first_name = $row['first_name'];
           $object->last_name  = $row['last_name'];
          
           $object_array[] = $object;
          
        }
        // получился массив из объектов (User Object)
        print_r($object_array);
        
        // $object в цикле while перезаписывается с каждой новой итерацией: 
        print_r($object); 
        // обращение к разным $object реализуется индексами $object_array

?>	
        </pre>
		</div>
		
    <div id="footer">Copyright <?php echo date("Y", time()); ?>, Kevin Skoglund</div>
  </body>
</html>