<?php

class DatabaseObject {

    protected static $table_name;         // необязательное добавление, атрибут используется в подклассах
    protected static $db_fields=array();  // необязательное добавление, атрибут используется в подклассах
    
      // Здесь используется позднее статическое связывание: static вместо self, -
      // здесь вызываются методы подкласса, расширяющие этот класс,
      // атрибут $table_name также берётся из подкласса, а не здесь.
      
      /** @return array of all objects */
    public static function find_all() {
      return static::find_by_sql("SELECT * FROM ".static::$table_name);
    }
    
    /**
     * @return object|false
     *  
     * */ 
    public static function find_by_id($id=0) {
      global $database;
        $result_array = static::find_by_sql("SELECT * FROM ".static::$table_name." WHERE id=".$database->escape_value($id)." LIMIT 1");
        return !empty($result_array) ? array_shift($result_array) : false ;
    }
    
    
      // Делается обычный запрос к БД,
      // получаем массив по каждой строке из БД ($row),
      // предоставляем этот массив (строку) методу instantiate(),
      // к-рый возвращает объект на базе этой строки (поля из БД присваиваются аналогичным атрибутам объекта),
      // в цикле while получившиеся объекты набиваются в массив $object_array[],
      // в итоге возвращается массив из объектов $object_array[] .

      // returns array of objects
    public static function find_by_sql($sql="") {
          global $database;
          $result_set = $database->query($sql);
      $object_array = array();
          while ($row = $database->fetch_array($result_set)) {
            $object_array[] = static::instantiate($row);
          }
          // массив из объектов
          return $object_array;
    }

    /**
     * @return string
     */
    public static function count_all() {
        global $database;
        $sql = "SELECT COUNT(*) FROM ".static::$table_name;
        $result_set = $database->query($sql);
        // массив
        $row = $database->fetch_array($result_set);
        // строка
        return array_shift($row);
    }
      
      // предоставляется массив (row, mix array from db)
      // возвращается объект этого класса
    public static function instantiate($record) {
          // Простой, но долгий способ:
          // нужно проверять каждую запись в БД
          // на то, что она существует и является массивом,
          // и необходимо сюда добавить связь с каждым столбцом из БД,
          // но их может быть и 50
          $object = new static;
          
      // for Late Static Binding possible:   
      //    $class_name = get_called_class();
      //    $object = new $class_name;        
          
      //    $object->id         = $record['id'];
      //    $object->username   = $record['username'];
      //    $object->password   = $record['password'];
      //    $object->first_name = $record['first_name'];
      //    $object->last_name  = $record['last_name'];
      
      // Короткий, динамический способ
          foreach($record as $attribute=>$value){
              if($object->has_attribute($attribute)) {
                $object->$attribute = $value;
              }
          }
                
          return $object;
    }
      
      // должно быть private, для отладки public
    private function has_attribute($attribute) {
      // get_object_vars returns an associative array with all attributes 
      // (incl. private ones!) as the keys and their current values as the value
      
        //$object_vars = get_object_vars($this);
      // или, что то же самое
        $object_vars = $this->attributes();

        // We don't care about the value, we just want to know if the key exists
      // Will return true or false

      return array_key_exists($attribute, $object_vars);
    }

      
      // для тестов public, изначально protected
      // не эскэйпенные атрибуты заданного объекта, массив
    public function attributes() {
      // return an assoc. array of attribute keys and their values
      // return get_object_vars($this);
      // get_object_vars имеет тот недостаток, что выбирает все свойства объекта,
      // вместо него можно перебирать нужные поля, перечисленные в начале подкласса:
      
      	// return an array of attribute names and their values
        $attributes = array();
        foreach(static::$db_fields as $field) {
          if(property_exists($this, $field)) {
            $attributes[$field] = $this->$field;
          }
        }
        return $attributes;

    }

      // для тестов public, изначально protected
      // эскэйпенные атрибуты заданного объекта, массив
    public function sanitized_attributes() {
      global $database;
      $clean_attributes = array();
      // sanitize the values before submitting
      // Note: does not alter the actual value of each attribute
      foreach($this->attributes() as $key => $value){
        $clean_attributes[$key] = $database->escape_value($value);
      }
      return $clean_attributes;
    }

    public function save() {
        // A new record won't have an id yet.
        // Запись в БД либо обновляется, либо создаётся заново 
        // в зависимости от того, существует ли уже id в БД или нет.
        // create() и update() можно поменять на private function.
        return isset($this->id) ? $this->update() : $this->create();
    }

    public function create() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        
        $attributes_with_id = $this->sanitized_attributes();
        array_shift($attributes_with_id);
        $attributes = $attributes_with_id; // уже без первого атрибута id
        // такой ход необходим, т.к. id попадает в SQL запрос вставки, причём с пустым значением,
        // а т.к. в БД идет автоматическое приращение, то id тут совсем не нужен

        $sql = "INSERT INTO ".static::$table_name." ("; // static берет значение свойства table_name из подкласса

        // вместо этого $sql .= "username, password, first_name, last_name";
        $sql .= join(", ", array_keys($attributes));

        $sql .= ") VALUES ('";

        // вместо, например, этого
        /*
        $sql .= $database->escape_value($this->username) ."', '";
        $sql .= $database->escape_value($this->password) ."', '";
        $sql .= $database->escape_value($this->first_name) ."', '";
        $sql .= $database->escape_value($this->last_name) ."')";
        */
        $sql .= join("', '", array_values($attributes));
        $sql .= "')";

        if($database->query($sql)) {
            $this->id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }

    public function update() {
          global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach($attributes as $key => $value) {
          $attribute_pairs[] = "{$key}='{$value}'";
        }

        $sql = "UPDATE ".static::$table_name." SET "; // static берет значение свойства table_name из подкласса

        // вместо, например, этого
        /*
        $sql .= "username='". $database->escape_value($this->username) ."', ";
        $sql .= "password='". $database->escape_value($this->password) ."', ";
        $sql .= "first_name='". $database->escape_value($this->first_name) ."', ";
        $sql .= "last_name='". $database->escape_value($this->last_name) ."' ";      
        */
        $sql .= join(", ", $attribute_pairs);

        $sql .= " WHERE id=". $database->escape_value($this->id);

        $database->query($sql);
        return ($database->affected_rows() == 1) ? true : false;
    }
      
    /**
     * @return true|false удаляет SQL-запросом и возвращает булево
     */
    public function delete() {
        global $database;
      // Don't forget your SQL syntax and good habits:
      // - DELETE FROM table WHERE condition LIMIT 1
      // - escape all values to prevent SQL injection
      // - use LIMIT 1
      $sql = "DELETE FROM ".static::$table_name; // static берет значение свойства table_name из подкласса
      $sql .= " WHERE id=". $database->escape_value($this->id);
      $sql .= " LIMIT 1";
      $database->query($sql);
      return ($database->affected_rows() == 1) ? true : false;
    
        // NB: After deleting, the instance of User still 
        // exists, even though the database entry does not.
        // This can be useful, as in:
        //   echo $user->first_name . " was deleted";
        // but, for example, we can't call $user->update() 
        // after calling $user->delete().
    }

}


